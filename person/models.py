from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save


class Encausado(models.Model):
    nombres = models.CharField(max_length=50)
    apellidos = models.CharField(max_length=50)
    cedula = models.CharField(max_length=8)
    fecnac = models.DateField(auto_now=False, auto_now_add=False, null=True)
    genero = models.CharField(max_length=20, null=True, blank=True)
    direccion = models.TextField(max_length=400)
    componente = models.ForeignKey('Componente', null=True, blank=True)
    grado = models.CharField(max_length=20, null=True, blank=True)
    unidad = models.CharField(max_length=20, null=True, blank=True)
    
    
    def __str__(self):  
          return "%s %s" %(self.nombres, self.apellidos) 

class Componente(models.Model):
    nombre = models.CharField(max_length=50, help_text='Ingrese el Nombre del Componente', verbose_name='Componente')
    
    def __unicode__(self):
        return self.nombre
    

class UserProfile(models.Model):  
    user = models.OneToOneField(User)  
    cedula = models.CharField(max_length=8)
    direccion = models.TextField(max_length=400)
    Impre = models.CharField(max_length=10)
    Unidad = models.CharField(max_length=50)
    
    def __str__(self):  
          return "%s's profile" % self.user  
        

def create_user_profile(sender, instance, created, **kwargs):  
    if created:  
       profile, created = UserProfile.objects.get_or_create(user=instance)  

post_save.connect(create_user_profile, sender=User) 
    
    


