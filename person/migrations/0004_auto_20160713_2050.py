# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-13 20:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0003_auto_20160708_1706'),
    ]

    operations = [
        migrations.CreateModel(
            name='Componente',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(help_text='Ingrese el Nombre del Componente', max_length=50, verbose_name='Componente')),
            ],
        ),
        migrations.AddField(
            model_name='encausado',
            name='genero',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='encausado',
            name='grado',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
        migrations.AddField(
            model_name='encausado',
            name='unidad',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
