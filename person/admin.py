from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

# Register your models here.
from .models import *

class UserProfileInline(admin.StackedInline):
    model = UserProfile
    can_delet = False
    verbose_name_plural = 'UserProfile'

class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )
    
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(Encausado)
admin.site.register(Componente)
